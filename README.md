# README #

A small script that prints out the names of the servers running in ElasticHosts account and the names of drives which they are using by calling the ElasticHosts API

Tested with and "Python v2.7.8" "Python v3.4.4rc1"

Also tested at http://rextester.com/l/python_online_compiler for Python v2.7.6 and at http://rextester.com/l/python3_online_compiler for Python v3.4.3

### How to use

You can create an instance of the TalkToElasticHostsAPI class, and call the print_server_disc_info method.

e.g.

```python
 o = TalkToElasticHostsAPI()
 o.print_server_disc_info()
```

By passing no arguments to the constructor, the default API url, credentials (user "rigasathanasios") and output type will be used.

You can pass the following arguments to the constructor:

**url** : <str> The API base URL

**user_uuid** : <str> Username

**secret_api_key** : <str> Password

**json_format** : <bool> Return json

e.g.

```python
o = TalkToElasticHostsAPI(
    url='https://api-lon-b.elastichosts.com',
    user_uuid='b88614ce-8eb1-4de5-a7f9-f8741dee255f',
    secret_api_key='tc7g3MkCM2wwjZebug8ygA2mk7JCMYkwcTJB366z',
    json_format=True
)
o.print_server_disc_info()
```

**And the output:**

```
Server name: srv1 | Discs: srv1-disc, srv4-disc
Server name: srv2 | Disc : srv2-disc
Server name: srv3 | No disc mounted on this server.
```