"""
A small script that prints out the names of the servers running in ElasticHosts
account and the names of drives which they are using by calling the
ElasticHosts API

Tested with and "Python v2.7.8" "Python v3.4.4rc1"

Also tested at http://rextester.com/l/python_online_compiler for
Python v2.7.6 and at http://rextester.com/l/python3_online_compiler for
Python v3.4.3
"""

from __future__ import print_function

try:
    from urllib.request import Request, urlopen, HTTPBasicAuthHandler, \
        build_opener, install_opener
    from urllib.error import HTTPError, URLError
except ImportError:
    from urllib2 import Request, urlopen, HTTPError, URLError, \
        HTTPBasicAuthHandler, build_opener, install_opener

import json
import re


class TalkToElasticHostsAPI:
    """
    A class to talk to ElasticHosts API
    """

    USER_AGENT = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; de; ' \
                 'rv:1.9.1.5) Gecko/20091102 Firefox/3.5.5'

    def __init__(self, url=None, user_uuid=None, secret_api_key=None,
                 json_format=False):
        """
        The constructor
        """

        if url is None:
            self.base_url = 'https://api-lon-b.elastichosts.com'
        else:
            self.base_url = url

        self.full_server_info_endpoint = '{}{}'.format(self.base_url,
                                                       '/servers/info/full')

        self.full_drive_info_endpoint = '{}{}'.format(self.base_url,
                                                      '/drives/info/full')

        if user_uuid is None:
            self.username = 'b88614ce-8eb1-4de5-a7f9-f8741dee255f'
        else:
            self.username = user_uuid

        if secret_api_key is None:
            self.password = 'tc7g3MkCM2wwjZebug8ygA2mk7JCMYkwcTJB366z'
        else:
            self.password = secret_api_key

        if not json_format:
            self.format = 'text/plain'
        else:
            self.format = 'application/json'

        self.headers = {
            'User-agent': self.USER_AGENT,
            'Content-Type': 'header',
            'Accept': self.format
        }

    def authenticate(self):
        """
        Create authentication handler
        """

        auth_handler = HTTPBasicAuthHandler()
        auth_handler.add_password(realm='API',
                                  uri=self.base_url,
                                  user=self.username,
                                  passwd=self.password)
        opener = build_opener(auth_handler)
        install_opener(opener)

    def make_request(self, url):
        """
        Make request
        :param url: str
        """

        # Call the authenticate method before doing anything
        self.authenticate()

        try:
            req = Request(url=url, headers=self.headers)
            response = urlopen(req)
            data = response.read().decode()

            if self.format == 'text/plain':
                return json.dumps(self.parse_text_response(data))
            else:
                return data
        except ValueError as e:
            print(e)

    def get_servers_details(self):
        """
        Get servers full details
        """

        request = self.make_request(self.full_server_info_endpoint)

        try:
            return json.loads(request)
        except ValueError as e:
            print(e)

    def get_drive_details(self):
        """
        Get drive full details
        """

        request = self.make_request(self.full_drive_info_endpoint)

        try:
            return json.loads(request)
        except ValueError as e:
            print(e)

    def correlate_servers_drives(self):
        """
        Correlate servers with the drives which they are using
        """

        try:
            server_details = self.prune_object(self.get_servers_details())
            drive_details = self.prune_object(self.get_drive_details())

            for obj in server_details:
                for k in obj.keys():
                    if isinstance(obj[k], list):
                        if len(obj[k]):
                            for n, i in enumerate(obj[k]):
                                for e in drive_details:
                                    if e['drive'] == i:
                                        obj[k][n] = e['name']
                        else:
                            obj[k] = None

            return server_details

        except HTTPError as err:
            if err.code == 400:
                print("Bad request!")
            elif err.code == 401:
                print("Access denied!")
            elif err.code == 402:
                print("Payment required!")
            elif err.code == 404:
                print("Page not found!")
            elif err.code == 405:
                print("Method not allowed!")
            elif err.code == 409:
                print("Drive, server or other resource is busy!")
            elif err.code == 500:
                print("Internal Server Error!")
            else:
                print("Something happened! Error code", err.code)
        except URLError as err:
            print("Some other error happened:", err.reason)

    def print_server_disc_info(self):
        """
        Print the server-disc(s) info
        """

        try:
            for srv in self.correlate_servers_drives():
                name = srv.get('name')
                disc_info = srv.get('disc_list')

                if disc_info:
                    disc_row_format = "{}, " * (len(disc_info))
                    print("Server name: {} | {:<5}: {}".format(
                        name, "Discs" if len(disc_info) > 1 else "Disc",
                        disc_row_format.format(*disc_info)).rstrip(', '))
                else:
                    print("Server name: {} | {}".format(
                        name, "No disc mounted on this server."))
        except TypeError as e:
            print(e)

        return 1

    @staticmethod
    def parse_text_response(data):
        """
        Parse API text/plain response
        :param data: str
        """

        # Create a list of lists. Each child list represents
        # our resources (servers, discs)
        obj = [i.split('\n') for i in data.split('\n\n')]

        # Create a two items list of each resource from the previous object.
        # e.g. [[['block:0:read:requests', '2840'],
        #        ['block:0:write:bytes', '4440064'], ....]]
        # Each two items list represents a resource's characteristic
        # e.g. ['mem', '256'] represents the memory amount
        # allocated to our server
        obj_resources = [[i.split(' ') for i in o if len(i)] for o in obj]

        # Create the final object to work with [json]
        data = [{t[0]: t[1] for t in o} for o in obj_resources]

        return data

    @staticmethod
    def prune_object(obj):
        """
        Extract the info that we want to worl with
        :param obj: dict
        """

        disc_matching_pattern = r'^\bblock\b:\d+$'
        pruned_obj = []

        for item in obj:
            disc_list = []
            pruned_item = {k: v for k, v in item.items()
                           if k in ['name', 'drive'] or
                           re.search(disc_matching_pattern, k)}

            for k, v in pruned_item.items():
                if k.startswith('block'):
                    disc_list.append(v)

            pruned_item = {k: v for k, v in pruned_item.items()
                           if not k.startswith('block')}

            if not pruned_item.get('drive'):
                pruned_item['disc_list'] = disc_list

            pruned_obj.append(pruned_item)

        return pruned_obj

if __name__ == "__main__":
    # Use: you can create an instance of the TalkToElasticHostsAPI class, and
    # call the print_server_disc_info method.
    # e.g. o = TalkToElasticHostsAPI()
    #      o.print_server_disc_info()

    # By passing no arguments to the constructor, the default API url,
    # credentials (user "rigasathanasios") and output type will be used.

    # You can pass the following arguments to the constructor:
    # url : <str> The API base URL
    # user_uuid : <str> Username
    # secret_api_key : <str> Password
    # json_format : <bool> Return json
    # e.g.
    #
    # o = TalkToElasticHostsAPI(
    #     url='https://api-lon-b.elastichosts.com',
    #     user_uuid='b88614ce-8eb1-4de5-a7f9-f8741dee255f',
    #     secret_api_key='tc7g3MkCM2wwjZebug8ygA2mk7JCMYkwcTJB366z',
    #     json_format=True
    # )
    #
    # o.print_server_disc_info()

    # Create an instance of TalkToElasticHostsAPI class [no arguments]
    srv_info = TalkToElasticHostsAPI()

    # Call the print_server_disc_info method of the class to print out servers
    # and discs
    srv_info.print_server_disc_info()
